let term = new Terminal();
term.open(document.getElementById('terminal'));


//focus the window
document.getElementsByClassName('xterm-helper-textarea')[0].focus();

function find_word_on_pos() {
    //fix issue with command completion
    //ls<tab> is broken
    let lastspace = 0;
    let nextspace = 0;
    let found = false;
    for (i = 0; i<=buf.length; i++) {
        if ((found && buf[i] == ' ') || pos==i) {
            nextspace = i;
            return [lastspace, nextspace];
        } else if (buf[i] == ' ') {
            lastspace = i;
        } else if (pos == i) {
            found = true;
        }
    }
    throw "Unreachable";
}


window.onresize = function () { 
    //height of cursor box = 9x18 (WxH)
    //Use html element.size attribute or window.size if that exists.
    //divide by 9x18, resize term to use that number rounded down
    let sh = window.innerHeight;
    let sw = window.innerWidth;
    //make me constant
    let n_col = sw/9;
    let n_row = sh/18;
    term.resize(Math.floor(n_col),Math.floor(n_row));
}
//call at the start so its not the default size
window.onresize();

let socket = new WebSocket(`ws://${document.location.host}/ws/`);
let pwd = "/";
//termlength is the length of the default terminal line when it is cd'd to ~
let termlength = 16;
//Total length is the length of buf.
let totallength = 16;
//let lines = [];
//line is the current line
//let line=0;
//Buf is the content the user has input
let buf = "";
//pos is the position in that line
let pos = 0;
//this is the CSI character for ansi or Esc+[
const CSI=String.fromCharCode(27,91);


socket.onmessage = function(event) {
        if (event.data.startsWith("cd:")) {
            pwd = event.data.slice(3);
            totallength = event.data.length-4+termlength;
            term.writeln('');
            prompt(term);
        } else if (event.data.startsWith("redir:")) {
            let loc = event.data.slice(6);
	    //messes up everything
            window.open(loc , '_blank');
            //window.location.href = `${loc}`;
        } else if (event.data.startsWith("complete:")) {
            let list_of_completes = event.data.slice(9).split(' ');
            switch (list_of_completes.length) {
                case 0:
                    //yeah we do nothing
                    break;
                case 1:
                    //pos needs to get fixed after this is done
                    let ret = find_word_on_pos()
                    let start = ret[0];
                    let end = ret[1];
                    let b1 = buf.slice(0,start+1);
                    let b2 = buf.slice(end-1,buf.length-1);
                    buf = b1+list_of_completes[0]+b2;
                    prompt(term);
                    term.write(buf);
                    break;
                default:
                    //implement me
            }
        } else if (event.data != "") {
            term.writeln('');
            term.write(event.data);
            prompt(term);
        }
};

socket.onclose = function(event) {
	if (event.wasClean) {
		term.writeln("Websocket connection closed");
	} else {
		term.writeln("Websocket connection died");
		prompt(term);
	}
};

socket.onerror = function(error) {
	term.writeln(`[websocket connection error] ${error.message}`);
	prompt(term);
};

//move to onopen
term.writeln('Welcome to website');
term.writeln('This is an emulated terminal');
term.writeln('Type \"help\" for a full list of commands');
term.writeln('Source code is at http://gitlab.com/liamnprg/command_line_website');
term.writeln('');
prompt(term);


//at around this point, the code gets very strange. xterm.js emulates an ANSI accepting terminal. Ansi sequences are used well througought the next section of code. This code reads more like C than JS in many parts. 


//CSI+4h ansi code, Esc[4h=changes terminal to insert mode instead of replace mode
term.write(CSI+"4h");

term.onData(e => {
switch (e) {
    case '\r': // Enter
        console.log("Sending command to backend");
        socket.send(buf);
        //lines.push(buf);
        buf = "";
        pos = 0;
        break;
    case '\u0003': // Ctrl+C
        prompt(term);
        buf = "";
        pos = 0;
        break;
    case '\u0009': // tab
        //secret command, tells the server to return a completion for the word
        console.log("Someone tabbed: OOPS");
        let split = buf.split(' ');
        let word = split.pop();
        socket.send(`complete ${word}`);
        break;
    case '\u007F': // Backspace (DEL)
        // Do not delete the prompt
        if (term._core.buffer.x >= totallength ) {
            //go back one, delete character
            term.write('\b'+CSI+'P');
        }
        //remove character from buf
        buf=buf.substring(0,pos-1)+buf.substring(pos);
        pos=pos-1;
        console.log(buf);

    break;
        //this wierd code is explaned by ansi sequences. CSI+A= the code for up arrow
    case CSI+'A' :  //up arrow
        console.log("cursor moved up");
        break;
    case CSI+'B' :  //down arrow
        console.log("cursor moved down");
        break;
    case CSI+'C' :  //right arrow
        console.log("cursor moved right");
        if (pos != buf.length) {
            //move the cursor right/left
            term.write(e);
            pos=pos+1;
                

        }
        break;
    case CSI+'D' :  //left arrow
        console.log("cursor moved left");
        if (pos!=0) {
            //move the cursor right/left
            term.write(e);
            pos=pos-1
        }
        break;
// add case for insert character or add case for other misc nonprint characters to delete them from buf
    default: 
        term.write(e);
        // console.log(e.charCodeAt(0));
        // console.log(e.charCodeAt(1));
        // console.log(e.charCodeAt(2));
        // console.log(e.charCodeAt(3));
        pos=pos+1;
        // insert at location
        buf = buf.substring(0,pos-1) + e + buf.substring(pos-1);
        console.log(buf);

}
});

//erases prompt 
function reprompt(term) {
    term.write(`\rwebsite :: ${pwd} $ `);
}
function prompt(term) {
    term.write(`\r\nwebsite :: ${pwd} $ `);
}
