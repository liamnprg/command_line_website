use actix::{Actor, StreamHandler};
use std::path::PathBuf; 
use actix_web::{web, App, Error, HttpRequest, HttpResponse, HttpServer};
use std::ffi::OsStr;
use actix_web_actors::ws;
use actix_files::Files;
use std::fs::read_dir; use std::path::Path;
use rustyline::completion::FilenameCompleter;

/// Define http actor
struct MyWs {
    cwd: Cwd,
    
}
//also derive Display here
#[derive(Clone)]
struct Cwd {
    //cwd always begins with ./front
    cwd: PathBuf,
}

impl Cwd {
    //safe, moves to parent directory
    fn up_one(&mut self) {
        if self.cwd == Path::new("./front") {
            return;
        }
        self.cwd.pop();
    }
    //takes self and adds a path onto it, returning once done, boundry safe, will not return files
    //outside of ./front
    fn file_path(&self, file: &str) -> PathBuf {
        let mut cwd = self.clone();
        let path = Path::new(file);
        for i in path.into_iter() {
            match &*i.to_string_lossy() {
                ".." => {
                    //safe, will not up past ./front
                    cwd.up_one()
                },
                "." => {},
                _ => { 
                    cwd.cwd.push(i)
                },
            }
        }
        cwd.cwd
    }
    //cwd without the prefix, usually used for printing
    fn prefixless(&self) -> String {
        //safe because why would there not be the ./front prefix
        String::from("/")+&*self.cwd.strip_prefix("./front").unwrap().to_string_lossy()

    }
    //returns to / 
    fn to_top(&mut self) {
        self.cwd = PathBuf::from("./front/");
    }
    //boundry safe, checks if cwd+path is a valid DIRECTORY to cd to, returns io::error if not the
    //case. returns a boolean, true if cd was done, false if error
    fn cd(&mut self, path: &str) -> bool {
        let newpath = self.file_path(path);
        if newpath.is_dir() {
            self.cwd = newpath;
            true
        } else {
            false
        }
    }
}


impl Actor for MyWs {
    type Context = ws::WebsocketContext<Self>;
}

/// Handler for ws::Message message
impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for MyWs {
    fn handle(
        &mut self,
        msg: Result<ws::Message, ws::ProtocolError>,
        ctx: &mut Self::Context,
    ) {
        match msg {
            Ok(ws::Message::Ping(msg)) => ctx.pong(&msg),
            Ok(ws::Message::Text(text)) => ctx.text(handle_command(self, text)),
            Ok(ws::Message::Binary(bin)) => ctx.binary(bin),
            _ => (),
        }
    }
}

fn handle_command(mut wsctx: &mut MyWs, text: String) -> String {
    let mut vec = Vec::new();
    //if we have a single/double quote
    let mut single = false;
    let mut double = false;
    //the current token
    let mut token = String::new();
    //if we just finished a quotation mark, useful for not parsing the space after a quote as a
    //separate token
    let mut finished_quote = false;

    for c in text.chars() {
        if c == '\"' {
            if double == true {
                token.push(c);
                vec.push(token.clone());
                token = "".to_string();
                double = false;
                finished_quote = true;
            } else if single == true {
                return String::from("Parser error: Too many layers of quotation marks");
            } else {
                token.push(c);
                double = true;
                finished_quote = false;
            }
            
        } else if c == '\'' {
            if single == true {
                token.push(c);
                vec.push(token.clone());
                token = "".to_string();
                single = false;
                finished_quote = true;
            } else if double == true {
                return String::from("Parser error: Too many layers of quotation marks");
            } else {
                token.push(c);
                single = true;
                //we always set this to false because we haven't finished a quote
                finished_quote = false;
            }
        } else if c == ' ' {
            if double == true || single == true {
                token.push(c);
            } else if finished_quote == true {
                //do nothing because we don't want to parse spaces after quotes
                finished_quote=false;
            } else {
                //set it again, not sure if this is nessecary 
                finished_quote = false;
                vec.push(token.clone());
                token = "".to_string();
            }

        } else {
            token.push(c);
        }
    }
    //do not parse space after quote as new token
    if finished_quote == false {
        //push the last token
        vec.push(token.clone());
    }
    if double == true || single == true {
        return String::from("Parser error: Unfinished quotation mark");
    }
    get_command_results(&mut wsctx, vec)
}

fn get_command_results(wsctx: &mut MyWs, args: Vec<String>) -> String {
    match args[0].as_str() {
        "help" => {
            let newline = std::str::from_utf8(&[27,91,69]);
            format!("This is an emulated terminal which sends commands to the backend via websockets. The commands are \"run\" by calling functions in the code that emulate common unix functions like cd and ls. The full list of functions is below, run the commands as normal to get extended help:\r

            help -- explains the system\r
            cd   -- change directory\r
            ls   -- list directory\r
            pwd  -- print current directory\r
view/vi/read/etc -- redirect to file\r
            ")
        }
        "ls" => {
            let argpath= {
                if args.len() > 1 {
                    args[1].clone()
                } else {
                    String::new()
                }
            };
            //append
            let path = wsctx.cwd.file_path(&argpath);
            if !path.is_dir() {
                return format!("ERROR DURING LS MORON FIX ME");
            }
            let rdu = read_dir(&path);

            if let Ok(liter) = rdu {
                let collected_result: Result<Vec<_>, std::io::Error> = liter.collect();
                let result = collected_result.unwrap();
                let paths: Vec<std::path::PathBuf> = result.iter().map(|direntry| direntry.path()).collect();

                let strippaths = paths.iter().map(|pathbuf| (&pathbuf).strip_prefix(&path)).collect::<Result<Vec<&Path>,std::path::StripPrefixError>>().unwrap();
                let mut col= String::new();
                for e in strippaths {
                    col=col+" "+e.to_str().unwrap();
                }
                col

            } else {
                return String::from("ls:error listing directory");
            }
            
        }
        "cd" => {
            let dest= {
                if args.len() > 1 {
                    &args[1]
                } else {
                    wsctx.cwd.to_top();
                    return String::new();
                        
                }
            };
            if !wsctx.cwd.cd(dest) {
                return format!("BIG STUPID DURING CD");
            }

            
            format!("cd:{}",wsctx.cwd.prefixless())
        }
        "pwd" => {
            wsctx.cwd.prefixless()
        }
        "view"|"read"|"vi"|"vim"|"nano"|"emacs"|"open"|"cat" => {
            let dest= {
                if args.len() > 1 {
                    &args[1]
                } else {
                    return String::from("error, cannot stat nothing");
                        
                }
            };

            let mut prefixless = wsctx.cwd.clone();
            prefixless.cwd = prefixless.file_path(&dest);
            let display = prefixless.prefixless();

            if !prefixless.cwd.is_file() {
                return format!("cannot read {}", display )
            } else {
                return format!("redir:{}",display);
            }
        }
        "complete" => {
            //use rustyline to complete this
            let completer = FilenameCompleter::new();
            //file_path(&self, file: &str) -> PathBuf

            let fp = wsctx.cwd.file_path(&args[1]);
            let fps = fp.to_string_lossy();
            println!("Completing {}", &fps);
            //./front/ is 9 characters long
            //take uncompleted string, remove last component("to be completed"), then calculate
            //length of the directories, pass that on to complete_path, then return and process
            //results

            let parent = fp.parent().unwrap();
            let parstr: &OsStr = parent.as_ref();
            //the rare off-by-two error!
            let parlen = parstr.len()+2;
            println!("{} parlen, {:?} parstr", parlen,parstr);

            //fix bug with nonexistent complete info, check input security
            let vecpairs = completer.complete_path(&fps,parlen).unwrap().1;
            if vecpairs.len() > 0 {
                format!("complete:{}", vecpairs[0].display)
            } else { 
                String::from("complete:")
            }
        }
        _ => format!("command {} not found", args[0])
    }


}

async fn ws_index(req: HttpRequest, stream: web::Payload) -> Result<HttpResponse, Error> {
    let resp = ws::start(MyWs {cwd: Cwd { cwd: PathBuf::from("./front/")}}, &req, stream);
    println!("Received websockets request");
    resp
}


#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    println!("Starting server on 0.0.0.0:8000");
    HttpServer::new(|| App::new()
                    .route("/ws/", web::get().to(ws_index))
                    .service(Files::new("/", "./front")
                             .index_file("index.html")
                             )
        )
        .bind("0.0.0.0:8000")?
        .run()
        .await
}
